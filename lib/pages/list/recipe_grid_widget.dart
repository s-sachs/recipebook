import 'package:flutter/material.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/details/recipe_details.dart';
import 'package:recipe_stornado/pages/list/recipe_grid_tile.dart';

class RecipeGridWidget extends StatelessWidget {
  late final List<Recipe>? recipes;

  RecipeGridWidget({required this.recipes}) {
    if (recipes == null) recipes = [];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: GridView.builder(
        padding: EdgeInsets.only(top: 10.0),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 4/5,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
        ),
        itemCount: recipes!.length,
        itemBuilder: (context, index) {
          return Container(
            child: InkWell(
              onTap: () {
                _tap(context, index);
              },
              child: RecipeGridTile(recipe: recipes![index])/*GridTile(
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                   Flexible(
                      flex: 4,
                      child: AspectRatio(
                        aspectRatio: 16/9,
                        child: Container(
                          child: recipes![index].picture == null
                              ? Image(image: AssetImage("assets/images/default_detail_picture.jpg"), fit: BoxFit.fitWidth)
                              : Image.memory(recipes![index].picture!.picture!, fit: BoxFit.fitWidth),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: Text(recipes![index].title.toString()),
                      ),
                    ),
                  ],
                ),
              ),
            ),*/
            ),
          );
        },
      )
    );
  }

  void _tap(BuildContext context, int index) {
    Navigator.push(context,MaterialPageRoute(builder: (context) => RecipeDetailsScreen(recipe: recipes![index])));
  }
}
