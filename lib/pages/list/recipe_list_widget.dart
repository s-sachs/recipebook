import 'package:flutter/material.dart' hide Step;
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/details/recipe_details.dart';
import 'package:recipe_stornado/util/meta_helpers.dart';

class RecipeListWidget extends StatelessWidget {

  late final List<Recipe>? recipes;

  RecipeListWidget({required this.recipes}) {
    if (recipes == null) recipes = [];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: recipes!.length,
        itemBuilder: (context, index) {
          Recipe recipe = recipes![index];

          return InkWell(
            onTap: () {
              _tap(context, index);
            },
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: ListTile(
                tileColor: Colors.transparent.withAlpha(50),
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    recipe.picture == null
                        ? Text((index + 1).toString(), style: Theme.of(context).textTheme.headline4,)
                        : Image.memory(recipe.picture!.picture!, width: 50, height: 50,),
                  ]
                ),
                title: Text(recipe.title.toString()),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(recipe.description.toString(),
                      style: TextStyle(color: Colors.white,),
                    ),
                    Text("Time: " + TimeHelper.minutesToFormattedString(recipe.timeTotal))
                  ],
                ),
              ),
            ),
          );
        }
      ),
    );
  }

  void _tap(BuildContext context, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) =>RecipeDetailsScreen(recipe: recipes![index])));
  }

}
