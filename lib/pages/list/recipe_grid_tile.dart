import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:recipe_stornado/models/recipe.dart';

class RecipeGridTile extends StatelessWidget {

  final Recipe recipe;

  const RecipeGridTile({Key? key, required this.recipe}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Image image = recipe.picture == null
      ? Image(
          image: AssetImage("assets/images/default_detail_picture.jpg"),
          fit: BoxFit.cover,
        )
      : Image.memory(
          recipe.picture!.picture!,
          fit: BoxFit.cover,
        );

    return GridTile(
      child: Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(10),
          ),
          /*border: Border.all(
            width: 3,
            color: Colors.red,
            style: BorderStyle.solid,
          ),*/
          color: Colors.white.withAlpha(150),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            AspectRatio(
              aspectRatio: 4/3,
              child: Container(
                color: Colors.white,
                child: image,
              ),
            ),

            SizedBox(height: 5),

            Text(
              recipe.title.toString(),
              style: Theme.of(context).textTheme.bodyText2!.copyWith(fontSize: 16),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),


            Expanded(
              //flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RatingBarIndicator(
                      rating: recipe.rating??0,
                      itemBuilder: (context, index) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      unratedColor: Colors.black38,
                      itemCount: 5,
                      itemSize: 16
                  ),

                  Row(
                    children: getIngIcons(),
                  ),
                ],
              ),
            ),




          ]
        ),
      ),
    );
  }

  List<Widget> getIngIcons() {
    final double size = 20;
    List<Widget> list = [];

    list.add(ImageIcon(
      AssetImage("assets/icons/icons8-pig-50.png"),
      color: colorContains("pig"),
      size: size,
    ));
    list.add(ImageIcon(
      AssetImage("assets/icons/icons8-chicken-50.png"),
      color: colorContains("chicken"),
      size: size,
    ));
    list.add(ImageIcon(
      AssetImage("assets/icons/icons8-cow-50.png"),
      color: colorContains("cow"),
      size: size,
    ));
    list.add(ImageIcon(
      AssetImage("assets/icons/icons8-vegetarian-mark-50.png"),
      color: colorContains("vegetarian"),
      size: size,
    ));

    return list;
  }

  Color colorContains(String str) {
    return (recipe.tags??[]).contains(str) ? Colors.green : Colors.grey;
  }

}
