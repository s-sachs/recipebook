import 'package:flutter/material.dart' hide Step;
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/list/recipe_grid_widget.dart';
import 'package:recipe_stornado/pages/list/recipe_list_widget.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';

class RecipeListScreen extends StatefulWidget {
  @override
  _RecipeListScreenState createState() => _RecipeListScreenState();
}

class _RecipeListScreenState extends State<RecipeListScreen> {

  bool list = false;

  @override
  Widget build(BuildContext context) {
    RecipeProvider provider = Provider.of<RecipeProvider>(context, listen: false);

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Recipe List"),

              Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () {
                      setState(() {});
                    },
                  ),
                  IconButton(
                    icon: list?Icon(Icons.view_list):Icon(Icons.grid_view),
                    onPressed: changeView,
                  ),
                ],
              ),

            ],
          ),
        ),
        body: FutureBuilder(
          future: provider.getRecipes(),
          builder: (BuildContext context, AsyncSnapshot<List<Recipe>> snapshot) {
            switch(snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
              default:
                if (snapshot.hasError) {
                  return Text("Error: ${snapshot.error}");
                } else {
                  if(snapshot.data == null || snapshot.data!.isEmpty) {
                    return Center(
                      child: Text("There are no recipes here yet"),
                    );
                  }
                  if (list) return RecipeListWidget(recipes: snapshot.data);
                  return RecipeGridWidget(recipes: snapshot.data);
                }
            }
          },
        )
      )
    );
  }

  void changeView() {
    setState(() {
      list = !list;
    });
  }

}
