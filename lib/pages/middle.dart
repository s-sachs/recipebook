import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MiddleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    a();
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                children: [
                  Text("https://icons8.com")
                ],
              );
            },
            icon: ImageIcon(
              AssetImage("assets/icons/icons8-pig-50.png"),
            ),
            //icon: Icon(Icons.info),
          ),

          AspectRatio(
            aspectRatio: 1/1,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(300.0),
              child: Image(
                image: AssetImage("assets/images/penguin.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),


          Expanded(
            child: FutureBuilder(
              future: a(),
              builder: (BuildContext context, AsyncSnapshot<Uint8List> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Image.memory(snapshot.data!);
                } else {
                  return CircularProgressIndicator();
                }
              }
            )
          ),
        ],
      ),
    );
  }

  Future<Uint8List> a() async {
    return ((await rootBundle.load("assets/images/penguin1.jpg")).buffer.asUint8List());
  }

}
