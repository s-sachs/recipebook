import 'package:flutter/material.dart' hide Step;
import 'package:recipe_stornado/models/recipe.dart';

class DetailsInstructionsWidget extends StatefulWidget {

  final Recipe recipe;

  const DetailsInstructionsWidget({Key? key, required this.recipe}) : super(key: key);

  @override
  _DetailsInstructionsWidgetState createState() => _DetailsInstructionsWidgetState(recipe: this.recipe);
}

class _DetailsInstructionsWidgetState extends State<DetailsInstructionsWidget> {

  final Recipe recipe;

  _DetailsInstructionsWidgetState({required this.recipe});



  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [

          Text("Instructions"),

          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: (recipe.instructions??[]).length,
            itemBuilder: (context, index) {
              return ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  recipe.instructions![index].title != null
                      ? Container(
                        child: Text(
                          recipe.instructions![index].title.toString(),
                          style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.bold),
                          textAlign: TextAlign.start,
                        )
                      )
                      : SizedBox.shrink(),

                  ListView.separated(
                    separatorBuilder: (context, i) {
                      return Divider(
                        color: Colors.grey,
                        thickness: 1,
                      );
                    },
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: ((recipe.instructions![index]).steps??[]).length,
                    itemBuilder: (context, outdex) {
                      Step step = recipe.instructions![index].steps![outdex];
                      return Text(
                        step.text.toString(),
                        maxLines: null,
                      );
                    },
                  )
                ],
              );
            },
          ),

        ],
      ),
    );
  }
}
