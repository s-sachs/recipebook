import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/create/create_recipe_screen.dart';

class DetailsMetaWidget extends StatelessWidget {

  final Recipe recipe;

  const DetailsMetaWidget({Key? key, required this.recipe}) : super(key: key);

  final Color beige = const Color.fromARGB(255, 255, 247, 227);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [

          AspectRatio(
            aspectRatio: 16/9,
          //SizedBox(
            //height: 200,
            child: ShaderMask(
              shaderCallback: (rect) {
                return LinearGradient(
                  begin: Alignment.center,
                  end: Alignment.bottomCenter,
                  colors: [Colors.black, Colors.transparent],
                ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
              },
              blendMode: BlendMode.dstIn,
              child: recipe.picture == null
                  //? Image.network("https://img.chefkoch-cdn.de/rezepte/1583241265803838/bilder/228652/crop-600x400/schupfnudel-hackfleisch-auflauf-mit-gemuese.jpg", fit: BoxFit.fitWidth)
                  ? Image(image: AssetImage("assets/images/default_detail_picture.jpg"), fit: BoxFit.fitWidth)
                  : Image.memory(recipe.picture!.picture!, fit: BoxFit.fitWidth),
            ),
          ),

          Container(
            //color: beige,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                children: [

                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          recipe.title.toString(),
                          style: Theme.of(context).textTheme.headline5!.copyWith(fontWeight: FontWeight.bold),
                          maxLines: null,
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 10.0),

                  // DESCRIPTION
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          recipe.description.toString(),
                          style: Theme.of(context).textTheme.subtitle1,
                          textAlign: TextAlign.start,
                        )
                      ),
                    ],
                  ),

                  SizedBox(height: 5.0),

                  // RATING
                  Row(
                    children: [
                      RatingBarIndicator(
                        rating: recipe.rating??0,
                        itemBuilder: (context, index) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        unratedColor: Colors.black38,
                        itemCount: 5,
                        itemSize: 20
                      ),

                      Expanded(
                        flex: 1,
                        child: Container(),
                      ),

                      Text(
                        recipe.rating.toString(),
                        style: Theme.of(context).textTheme.subtitle2,
                      ),

                      Expanded(
                        flex: 20,
                        child: Container(),
                      ),


                    ],
                  ),

                  // TIMES



                  IconButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipeScreen(recipe: recipe)));
                    },
                    icon: Icon(Icons.update),
                  ),


                ],
              ),
            ),
          ),

        ],
      )
    );
  }


}
