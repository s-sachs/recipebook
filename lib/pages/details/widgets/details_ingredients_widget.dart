import 'package:flutter/material.dart';
import 'package:recipe_stornado/models/recipe.dart';

class DetailsIngredientWidget extends StatefulWidget {

  final Recipe recipe;

  const DetailsIngredientWidget({Key? key, required this.recipe}) : super(key: key);

  @override
  _DetailsIngredientWidgetState createState() => _DetailsIngredientWidgetState(recipe: recipe);
}

class _DetailsIngredientWidgetState extends State<DetailsIngredientWidget> {

  final int defaultServings = 2;

  late final Recipe recipe;

  late int servings;

  _DetailsIngredientWidgetState({required Recipe recipe}) {
    this.recipe = recipe;
    this.servings = recipe.servings??defaultServings;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [

          // Servings Title


          // Servings Button
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Ingredients for $servings servings",
                style: Theme.of(context).textTheme.headline6,
              ),

              Row(
                children: [
                  IconButton(
                    onPressed: decreaseServings,
                    icon: Icon(Icons.remove),
                  ),

                  IconButton(
                    onPressed: increaseServings,
                    icon: Icon(Icons.add),
                  ),
                ],
              ),
            ],
          ),

          // Ingredients List
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: (recipe.ingredientsSubLists??[]).length,
            itemBuilder: (context, index) {
              return ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  // Title of a Sublist
                  recipe.ingredientsSubLists![index].title != null
                      ? Container(
                          //color: Colors.blue,
                          child: Text(
                            recipe.ingredientsSubLists![index].title.toString(),
                            style: Theme.of(context).textTheme.subtitle1!.copyWith(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.start,
                          )
                        )
                      : SizedBox.shrink(),

                  // Ingredients of a Sublist
                  ListView.separated(
                    separatorBuilder: (context, i) {
                      return Divider(
                        color: Colors.grey,
                        thickness: 1,
                      );
                    },
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: ((recipe.ingredientsSubLists![index]).ingredients??[]).length,
                    itemBuilder: (context, outdex) {
                      Ingredient ingredient = recipe.ingredientsSubLists![index].ingredients![outdex];

                      return Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Text(
                              getAmountString(ingredient.amount),
                              style: Theme.of(context).textTheme.subtitle1,
                              textAlign: TextAlign.end,
                            ),
                          ),
                          SizedBox(width: 5.0),
                          Expanded(
                            flex: 1,
                            child: Text(
                              (ingredient.unit??"").toString(),
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                          SizedBox(width: 5.0),
                          Expanded(
                            flex: 4,
                            child: Text(
                              getNameString(ingredient),
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),

                        ],
                      );
                    },
                  ),
                ],

              );
            }
          )
        ],
      )
    );
  }

  /// Returns the given amount as String
  /// Returns an empty String when the given amount is null
  /// Truncates trailing zeroes
  String getAmountString(num? amount) {
    if ((amount??0) == 0) return "";
    String result = ((amount! / (recipe.servings??defaultServings)) * servings).toStringAsFixed(2);
    RegExp regex = RegExp(r"([.]*0+)(?!.*\d)");
    /// RegExp regex = RegExp(r"([.]*0+)(?!.*\d)"); // removes every trailing zero
    result = result.replaceAll(regex, "");
    return result;
  }

  String getNameString(Ingredient ingredient) {
    String result = ingredient.name??"";
    if (ingredient.comment != null && ingredient.comment!.length > 0) result += ", " + ingredient.comment.toString();
    return result;
  }

  void increaseServings() {
    setState(() {
      servings++;
    });
  }

  void decreaseServings() {
    setState(() {
      servings--;
    });
  }
}
