import 'package:flutter/material.dart';
import 'package:recipe_stornado/models/recipe.dart';

class IngredientsWidget extends StatefulWidget {

  final Recipe recipe;

  IngredientsWidget(this.recipe);

  @override
  _IngredientsWidgetState createState() => _IngredientsWidgetState(recipe);
}

class _IngredientsWidgetState extends State<IngredientsWidget> {

  final int defaultServings = 2;

  late final Recipe recipe;

  late int servings;

  _IngredientsWidgetState(this.recipe) {
    servings = recipe.servings??defaultServings;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: (recipe.ingredientsSubLists??[]).length,
            itemBuilder: (context, index) {
              return ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: ((recipe.ingredientsSubLists![index]).ingredients??[]).length,
                itemBuilder: (context, outdex) {
                  Ingredient ingredient = recipe.ingredientsSubLists![index].ingredients![outdex];
                  return Text(getAmountString(ingredient.amount??0) + " - " + ingredient.unit.toString() +
                    " - " + ingredient.name.toString() + " , " + ingredient.comment.toString());
                },
              );
            },
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              
              IconButton(
                onPressed: () {
                  setState(() {
                    servings--;
                  });
                },
                icon: Icon(Icons.remove),
              ),

              Text(servings.toString()),

              IconButton(
                onPressed: () {
                  setState(() {
                    servings++;
                  });
                },
                icon: Icon(Icons.add),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget getServingsWidget(Widget child) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: child,
    );

  }

  String getAmountString(num amount) {
    if (amount == 0) return "";
    return ((amount / (recipe.servings??defaultServings)) * servings).toString();
  }

}
