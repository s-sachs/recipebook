import 'package:flutter/material.dart' hide Step;
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/details/ingredients_widget.dart';
import 'package:recipe_stornado/pages/details/widgets/details_ingredients_widget.dart';
import 'package:recipe_stornado/pages/details/widgets/details_instructions_widget.dart';
import 'package:recipe_stornado/pages/details/widgets/details_meta_widget.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';

class RecipeDetailsScreen extends StatelessWidget {

  final Recipe recipe;
  final double spacing = 10.0;
  final Color beige = const Color.fromARGB(255, 255, 247, 227);

  const RecipeDetailsScreen({required this.recipe});


  static List<Widget> _widgetOptions = <Widget>[

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: beige,
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            favoriteIcon(context),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                showAlertDialog(context);
              },
            ),
          ],
        ),
      ),
      body: Container(
        child: Container(
          child: ListView(
            children: [

              DetailsMetaWidget(recipe: recipe),

              DetailsIngredientWidget(recipe: recipe),

              DetailsInstructionsWidget(recipe: recipe),
             // IngredientsWidget(recipe),

            ],
          ),
        ),
      ),
    );
  }

  Widget favoriteIcon(BuildContext context) {
    RecipeProvider provider = Provider.of<RecipeProvider>(context, listen: false);
    return IconButton(
      onPressed: () async {
        await provider.toggleFavorite(recipe);
      },
      icon: FutureBuilder(
        future: provider.isFav(recipe),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if(snapshot.connectionState == ConnectionState.waiting) {
            return Icon(Icons.favorite_border);
          } else if (snapshot.hasData && snapshot.data!) {
            return Icon(Icons.favorite);
          } else {
            return Icon(Icons.favorite_border);
          }
        },
        initialData: false,
      ),
    );

    //RecipeProvider provider = Provider.of<RecipeProvider>(context, listen: false);
    bool isFavorite = provider.isFavorite(recipe);
    print(isFavorite.toString());
    return IconButton(
      icon: isFavorite ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
      onPressed: () async {
        await provider.toggleFavorite(recipe);
      },
    );
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget deleteButton = TextButton(
      child: Text("Delete", style: TextStyle(color: Colors.red)),
      onPressed: () async {
        print(await Provider.of<RecipeProvider>(context, listen: false).removeRecipe(recipe));
        Navigator.of(context).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Delete Recipe"),
      content: Column(
        children: [
          Text("Do you really want to delete this recipe?"),
          Text("This action cannot be undone!"),
        ],
      ),
      actions: [
        cancelButton,
        deleteButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      }
    );



  }

}

class RecipeDetailsScreenA extends StatelessWidget {

  final Recipe recipe;

  const RecipeDetailsScreenA({required this.recipe});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(recipe.title.toString()),
            ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.red[500]),
              onPressed: () async {
                await Provider.of<RecipeProvider>(context, listen: false).removeRecipe(recipe);
                Navigator.pop(context);
              },
              child: Text("Remove"),
            ),
          ],
        ),
      ),
      body: Container(
        child: ListView(
          children: [

            Text(recipe.id.toString()),
            Text(recipe.ownerId.toString()),

            Text(recipe.title.toString()),
            Text(recipe.description.toString()),
            Text(recipe.title.toString()),

            Text(recipe.rating.toString()),
            Text(recipe.difficulty.toString()),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(recipe.timePrep.toString()),
                Text(recipe.timeCook.toString()),
                Text(recipe.timeTotal.toString()),
              ],
            ),

            Text(recipe.servings.toString()),

            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: (recipe.ingredientsSubLists??[]).length,
              itemBuilder: (context, index) {

                return ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: ((recipe.ingredientsSubLists![index]).ingredients??[]).length,
                  itemBuilder: (context, outdex) {
                    Ingredient ingredient = recipe.ingredientsSubLists![index].ingredients![outdex];
                    return Text(ingredient.amount.toString() + " - " + ingredient.unit.toString() + " - " + ingredient.name.toString());
                  }
                );
              },
            ),

            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: (recipe.instructions??[]).length,
              itemBuilder: (context, index) {

                return ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: ((recipe.instructions![index]).steps??[]).length,
                    itemBuilder: (context, outdex) {
                      Step step = recipe.instructions![index].steps![outdex];
                      return Text(step.number.toString() + " - " + step.text.toString());
                    }
                );
              },
            ),

            Text(recipe.createdAt.toString()),
            Text(recipe.updatedAt.toString()),

            Text(recipe.version.toString()),

            IngredientsWidget(recipe),


          ],
        )
      ),
    );
  }
}