import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/create/create_recipe_screen.dart';
import 'package:recipe_stornado/pages/new/new_favorite_widget.dart';
import 'package:recipe_stornado/pages/new/new_list_widget.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';
import 'package:recipe_stornado/util/extractors/extractor.dart';
import 'package:recipe_stornado/util/sort/recipe_comparators.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Recipe Stornado"),
              Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () {
                      Provider.of<RecipeProvider>(context, listen: false).refreshRecipes();
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.download_outlined),
                    onPressed: () async {
                      await showAlertDialog(context);
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipeScreen()));
                    },
                  ),
                ],
              )
            ],
          ),
          bottom: TabBar(
            tabs: [
              Tab(text: "New"),
              Tab(text: "List"),
              Tab(text: "Favorites"),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            NewListWidget(comparator: RecipeComparators.createdComparator,),
            NewListWidget(comparator: RecipeComparators.nameComparator,),
            NewFavoriteWidget(),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    String url = "";
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget submitButton = TextButton(
      child: Text("Import", style: TextStyle(color: Colors.red)),
      onPressed: () async {
        Recipe? recipe = await Extractor.fetchRecipeFromUrl(url);
        Navigator.of(context).pop();
        Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipeScreen(recipe: recipe,)));
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Delete Recipe"),
      content: Column(
        children: [
          Text("Enter the chefkoch URL of the recipe you want to import"),
          TextField(
            onChanged: (String? value) {
              url = value??"";
            }
          )
        ],
      ),
      actions: [
        cancelButton,
        submitButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      }
    );
  }
}
