import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/pages/create/create_choice_screen.dart';
import 'package:recipe_stornado/pages/create/subscreens/create_tags_screen.dart';
import 'package:recipe_stornado/pages/list/recipe_list_screen.dart';
import 'package:recipe_stornado/pages/main_page.dart';
import 'package:recipe_stornado/pages/middle.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';
import 'package:recipe_stornado/scroll.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  static List<Widget> _widgetOptions = <Widget>[
    RecipeListScreen(),
    MainPage(),//CreateTagsScreen(),
    CreateChoiceScreen(),
  ];

  int _selectedIndex = 1;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    //Provider.of<RecipeProvider>(context, listen: false).init();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return MenuList();
    return MainPage();
    return Scaffold(
      body: Center(child:_widgetOptions.elementAt(_selectedIndex)),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.first_page),
            label: "A",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu),
            label: "B",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.last_page),
            label: "C",
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: (index) => _onItemTapped(index),
      ),
    );
  }
}
