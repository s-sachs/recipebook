import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/details/recipe_details.dart';
import 'package:recipe_stornado/pages/list/recipe_grid_tile.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';

class NewFavoriteWidget extends StatefulWidget {
  const NewFavoriteWidget({Key? key}) : super(key: key);

  @override
  _NewFavoriteWidgetState createState() => _NewFavoriteWidgetState();
}

class _NewFavoriteWidgetState extends State<NewFavoriteWidget> with AutomaticKeepAliveClientMixin {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    RecipeProvider provider = Provider.of<RecipeProvider>(context);

    return FutureBuilder(
        future: provider.getFavoriteRecipes(),
        builder: (BuildContext context, AsyncSnapshot<List<Recipe>> snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              if (snapshot.hasData) {
                if (snapshot.data!.length == 0) return Text("No recipes here :/");
                return RecipeGrid(recipes: snapshot.data!);
              }
              return Text("No recipes here :/");
            default:
              return Center(child:Text(snapshot.error.toString()));
          }
        }
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class RecipeGrid extends StatelessWidget {
  final List<Recipe> recipes;

  RecipeGrid({Key? key, required this.recipes}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 4/5,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
      ),
      itemCount: recipes.length,
      itemBuilder: (context, index) {
        return RecipeTile(recipe: recipes[index]);
      },
    );
  }
}

class RecipeTile extends StatelessWidget {
  final Recipe recipe;
  const RecipeTile({Key? key, required this.recipe}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => RecipeDetailsScreen(recipe: recipe)));
        },
        child: RecipeGridTile(recipe: recipe),
      ),
    );
  }
}
