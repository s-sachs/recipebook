import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/pages/new/new_list_widget.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';

class NewPage extends StatefulWidget {
  const NewPage({Key? key}) : super(key: key);

  @override
  _NewPageState createState() => _NewPageState();
}

class _NewPageState extends State<NewPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Recipes"),
              Row(
                children: [
                  IconButton(
                    onPressed: () {
                      Provider.of<RecipeProvider>(context, listen: false).refreshRecipes();
                    },
                    icon: Icon(Icons.refresh),
                  ),
                ],
              ),
            ],
          ),
          bottom: TabBar(
            tabs: [
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text("List"),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text("Favorites"),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            NewListWidget(),
            Center(
              child: Icon(Icons.star),
            ),
          ],
        ),
      ),
    );
  }
}
