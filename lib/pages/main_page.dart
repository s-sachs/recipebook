import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/create/create_recipe_screen.dart';
import 'package:recipe_stornado/pages/details/recipe_details.dart';
import 'package:recipe_stornado/pages/list/recipe_grid_tile.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  bool _showBackToTopButton = false;
  late ScrollController _scrollController;

  @override
  void initState() {
    // TODO: implement initState
    _scrollController = ScrollController()
      ..addListener(() {
        setState(() {
          if(_scrollController.offset > 400) {
            _showBackToTopButton = true;
          } else {
            _showBackToTopButton = false;
          }
        });
      });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollToTop() {
    _scrollController.animateTo(0, duration: Duration(seconds: 1), curve: Curves.linear);
  }


  @override
  Widget build(BuildContext context) {
    //return Icon(Icons.ad_units);
    return Scaffold(
      body: SafeArea(child: MyCustomScrollLView(recipesA: [], scrollController: _scrollController,)),
      floatingActionButton: _showBackToTopButton == false
        ? null
        : FloatingActionButton(
            onPressed: _scrollToTop,
            child: Icon(Icons.arrow_upward),
          ),
    );
  }

  void _tap(BuildContext context, Recipe recipe) {
    Navigator.push(context,MaterialPageRoute(builder: (context) => RecipeDetailsScreen(recipe: recipe)));
  }


}

class MyCustomScrollLView extends StatelessWidget {

  final List<Recipe> recipesA;
  final ScrollController scrollController;

  const MyCustomScrollLView({Key? key, required this.recipesA, required this.scrollController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RecipeProvider provider = Provider.of<RecipeProvider>(context);
    return FutureBuilder(
      future: provider.getRecipes(),
      builder: (BuildContext context, AsyncSnapshot<List<Recipe>> snapshot) {
        Widget sliverGrid;
        if (snapshot.hasData) {
          sliverGrid = MyCustomSliverGrid(recipesA: snapshot.data!);
        } else {
          sliverGrid = SliverToBoxAdapter(child: Center(child: CircularProgressIndicator()));
        }
        /*return NestedScrollView(
          controller: scrollController,
          headerSliverBuilder: (BuildContext context, bool isScrolled) {
            return <Widget>[
              SliverAppBar(
                //floating: false,
                stretch: true,
                //stretchTriggerOffset: 150.0,
                flexibleSpace: CustomFlexibleSpaceBar(),
                expandedHeight: MediaQuery.of(context).size.height * 6/10,
              ),
            ];
          },
          body: Container(
            child: ListView(children: List.generate(20, (e) => Text("A"))),
          ),
        );*/
        return CustomScrollView(
            cacheExtent: 100,
            controller: scrollController,
            slivers: [
              // Add the app bar to the CustomScrollView.
              SliverAppBar(
                //floating: false,
                flexibleSpace: CustomFlexibleSpaceBar(),
                expandedHeight: MediaQuery.of(context).size.height * 6/10,
              ),
              /*SliverToBoxAdapter(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 6 / 10,
                  child: Container(
                    color: Colors.green.shade200,
                    padding: EdgeInsets.only(top: 50),
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceAround,
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Spacer(flex: 2),
                        FlutterLogo(size: 80,),
                        Spacer(flex: 2),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Spacer(),
                            MyButton(
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipeScreen()));
                              },
                              icon: Icon(Icons.add),
                              text: "Add",
                            ),
                            Spacer(),
                            MyButton(
                              onPressed: () {},
                              icon: Icon(Icons.download_outlined),
                              text: "Import",
                            ),
                            Spacer(),
                          ],
                        ),
                        Spacer(flex: 2),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Spacer(),
                            MyButton(
                              onPressed: () async {
                                List<Recipe> recipes = await context.read<RecipeProvider>().getRecipes();
                                if (recipes.length > 0) {
                                  int min = 0;
                                  int max = recipes.length - 1;
                                  Random rnd = new Random();
                                  int r = 0 + rnd.nextInt(max - min);
                                  Recipe recipe = recipes[r];
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => RecipeDetailsScreen(
                                          recipe: recipe)));
                                }
                              },
                              icon: Icon(Icons.shuffle),
                              text: "Random",
                            ),
                            Spacer(),
                            MyButton(
                              onPressed: () {},
                              icon: Icon(Icons.settings),
                              text: "Settings",
                            ),
                            Spacer(),
                          ],
                        ),
                        Spacer(flex: 4),
                      ],
                    ),
                  ),
                ),
              ),*/
              SliverPadding(
                padding: EdgeInsets.only(top: 10),
              ),
              sliverGrid,
            ]
        );
      }
    );
  }
}



class MyCustomSliverGrid extends StatelessWidget {

  final List<Recipe> recipesA;

  const MyCustomSliverGrid({Key? key, required this.recipesA}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 4/5,
        mainAxisSpacing: 10,
        //crossAxisSpacing: 10,
      ),
      delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
          Recipe recipe = recipesA[index];

          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => RecipeDetailsScreen(recipe: recipe)));
              },
              child: RecipeGridTile(recipe: recipe),
            ),
          );
        },
        childCount: recipesA.length,
      ),
    );
  }
}



class CustomFlexibleSpaceBar extends StatelessWidget {
  const CustomFlexibleSpaceBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlexibleSpaceBar(
      collapseMode: CollapseMode.parallax,
      centerTitle: true,
      background: Container(
        color: Colors.green.shade200,
        padding: EdgeInsets.only(top: 50),
        //clipBehavior: Clip.hardEdge,
        child: ListView(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(flex: 2),
            FlutterLogo(size: 80,),
            Spacer(flex: 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Spacer(),
                MyButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipeScreen()));
                  },
                  icon: Icon(Icons.add),
                  text: "Add",
                ),
                Spacer(),
                MyButton(
                  onPressed: () {},
                  icon: Icon(Icons.download_outlined),
                  text: "Import",
                ),
                Spacer(),
              ],
            ),
            Spacer(flex: 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(),
                MyButton(
                  onPressed: () async {
                    List<Recipe> recipes = await context.read<RecipeProvider>().getRecipes();
                    if (recipes.length > 0) {
                      int min = 0;
                      int max = recipes.length - 1;
                      Random rnd = new Random();
                      int r = 0 + rnd.nextInt(max - min);
                      Recipe recipe = recipes[r];
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => RecipeDetailsScreen(
                              recipe: recipe)));
                    }
                  },
                  icon: Icon(Icons.shuffle),
                  text: "Random",
                ),
                Spacer(),
                MyButton(
                  onPressed: () {},
                  icon: Icon(Icons.settings),
                  text: "Settings",
                ),
                Spacer(),
              ],
            ),
            Spacer(flex: 4),
          ],
        ),
      ),
    );
  }
}

class MyButton extends StatelessWidget {

  final Function()? onPressed;
  final Icon icon;
  final String text;

  const MyButton({Key? key, this.onPressed, required this.icon, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 80,
      height: 80,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1.0),
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: TextButton(
          onPressed: onPressed,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              icon,
              Text(text.toString()),
            ],
          ),
        ),
      ),
    );
  }
}

