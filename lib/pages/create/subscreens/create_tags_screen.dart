import 'package:flutter/material.dart';
import 'package:recipe_stornado/provider/create_provider.dart';

class CreateTagsScreen extends StatefulWidget {

  final CreateProvider provider;

  const CreateTagsScreen({required this.provider});

  @override
  _CreateTagsScreenState createState() => _CreateTagsScreenState();
}

class _CreateTagsScreenState extends State<CreateTagsScreen> {

  Map<String, bool> tagMap = Map.fromIterable(tags,
    key: (item) => item.toString(),
    value: (item) => false,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Tag your Recipe"),
            IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                submit(context);
              },
            )
          ],
        )
      ),
      body: Container(
        child: ListView.builder(
          itemCount: tagMap.length,
          itemBuilder: (BuildContext context, int index) {
            return CheckboxListTile(
              title: Text(tagMap.keys.toList()[index].toString()),
              controlAffinity: ListTileControlAffinity.trailing,
              value: tagMap[tagMap.keys.toList()[index]],
              onChanged: (bool? value) {
                setState(() {
                  tagMap[tagMap.keys.toList()[index]] = value??false;
                });
              },
            );
          },
        ),
      ),
    );
  }

  submit(BuildContext context) {
    CreateProvider provider = widget.provider;
    if (provider.recipe.tags == null) {
      provider.recipe.tags = [];
    }
    print(provider.recipe.tags!.length);
    for (String key in tagMap.keys) {
      if (tagMap[key]!) {
        if (!provider.recipe.tags!.contains(key)) provider.recipe.tags!.add(key);
      }
    }
  }

  static final List<String> tags = [
    "cow",
    "pig",
    "chicken",

    "vegetarian",
    "vegan",

    "breakfast",
    "lunch",
    "dinner",
    "snack",

    "baking",
    "pastry",

    "hot",
    "cold",

    "fast",


    "test",
    "dev",

  ];


}

