import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/create/subscreens/create_solo_ingredient_widget.dart';
import 'package:recipe_stornado/provider/create_provider.dart';

class CreateIngredientsScreen extends StatefulWidget {
  @override
  _CreateIngredientsScreenState createState() => _CreateIngredientsScreenState();
}

class _CreateIngredientsScreenState extends State<CreateIngredientsScreen> with AutomaticKeepAliveClientMixin<CreateIngredientsScreen> {

  @override
  Widget build(BuildContext context) {
    super.build(context);

    CreateProvider provider = Provider.of<CreateProvider>(context);

    return Container(
      child: Scaffold(
        body: ListView(
          children: [

            TextFormField(
              initialValue: (provider.recipe.servings??"").toString(),
              decoration: InputDecoration(
                labelText: "Servings",
              ),
              keyboardType: TextInputType.number,
              validator: (String? value) {
                if (value == null) return "Enter a number";
                int? number = int.tryParse(value);
                if (number == null) return "Enter valid number";
                if (number <= 0) return "Number must be > 0";
                return null;
              },
              onSaved: (String? value) {
                provider.recipe.servings = int.parse(value??"2");
              },
            ),

            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: (provider.recipe.ingredientsSubLists??[]).length,
              itemBuilder: (context, index) {

                return ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: TextFormField(
                        initialValue: provider.recipe.ingredientsSubLists != null
                            ? (provider.recipe.ingredientsSubLists![index].title??"").toString()
                            : "",
                        decoration: InputDecoration(
                          labelText: "Title"
                        ),
                        onSaved: (String? value) {
                          provider.recipe.ingredientsSubLists![index].title = value;
                        },
                      ),
                    ),

                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: ((provider.recipe.ingredientsSubLists??[])[index].ingredients??[]).length,
                      itemBuilder: (context, outdex) {
                        Ingredient item = provider.recipe.ingredientsSubLists![index].ingredients![outdex];

                        return Container(
                          padding: EdgeInsets.symmetric(horizontal: 20.0),
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [

                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      child: Text(item.amount.toString(), textAlign: TextAlign.end,),
                                    ),
                                  ),
                                  SizedBox(width: 5.0),
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      child: Text(item.unit.toString()),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 8,
                                    child: Container(
                                      child: Text(item.name.toString() + ", " + item.comment.toString()),
                                    ),
                                  ),

                                ],
                              ),
                            ),
                          ),
                        );

                      },
                    ),

                    TextButton(
                      onPressed: () {
                        createIngredient(index, null);
                      },
                      child: Text("Add Ingredient")
                    ),

                  ],
                );


              },
            ),

            TextButton(
              onPressed: () {
                setState(() {
                  _addNewSubList();
                });
              },
              child: Text("Add List")),
          ],
        ),
      )
    );
  }

  void createIngredient(int index, Ingredient? ingredient) {
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CreateSoloIngredient(onSaved: addIngredientToSublist, index: index, initIngredient: ingredient,)));
  }

  void _addNewSubList() {
    CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);
    if (provider.recipe.ingredientsSubLists == null) provider.recipe.ingredientsSubLists = [];
    provider.recipe.ingredientsSubLists!.add(IngredientsSubList());
    print(provider.recipe.ingredientsSubLists!.length);
  }


  addIngredientToSublist(int sublistIndex, Ingredient ingredient) {
    setState(() {

      CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);

      if (provider.recipe.ingredientsSubLists == null) return;

      if (provider.recipe.ingredientsSubLists![sublistIndex].ingredients == null) provider.recipe.ingredientsSubLists![sublistIndex].ingredients = [];
      provider.recipe.ingredientsSubLists![sublistIndex].ingredients!.add(ingredient);

      print(provider.recipe.ingredientsSubLists![sublistIndex].ingredients![0].name.toString());

    });
  }



  @override
  bool get wantKeepAlive => true;
}
