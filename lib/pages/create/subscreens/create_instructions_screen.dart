import 'package:flutter/material.dart' hide Step;
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
//import 'package:recipe_stornado/models/recipe.dart' as r;
import 'package:recipe_stornado/provider/create_provider.dart';

class CreateInstructionsScreen extends StatefulWidget {
  @override
  _CreateInstructionsScreenState createState() => _CreateInstructionsScreenState();
}

class _CreateInstructionsScreenState extends State<CreateInstructionsScreen> with AutomaticKeepAliveClientMixin<CreateInstructionsScreen>{

  @override
  Widget build(BuildContext context) {
    super.build(context);

    CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);

    return Container(
      child: Scaffold(
        body: ListView(
          children: [

            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: (provider.recipe.instructions??[]).length,
              itemBuilder: (context, index) {

                return ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: TextFormField(
                        initialValue: provider.recipe.instructions != null
                          ? (provider.recipe.instructions![index].title??"").toString()
                          : "",
                        decoration: InputDecoration(
                          labelText: "Title",
                          hintText: "ex: \"making the sauce\"",
                        ),
                        onSaved: (String? value) {
                          provider.recipe.instructions![index].title = value;
                        }
                      ),
                    ),

                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: ((provider.recipe.instructions??[])[index].steps??[]).length,
                      itemBuilder: (context, outdex) {
                        Step item = provider.recipe.instructions![index].steps![outdex];

                        return Container(
                          //padding: EdgeInsets.all(20.0),
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(1),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [

                                  TextFormField(
                                    initialValue: item.text??"",
                                    decoration: InputDecoration(
                                      labelText: "Step #" + (outdex + 1).toString(),
                                    ),
                                    maxLines: null,
                                    onSaved: (String? value) {
                                      provider.recipe.instructions![index].steps![outdex].number = outdex + 1;
                                      provider.recipe.instructions![index].steps![outdex].text = value;
                                    },
                                  ),

                                ],
                              ),
                            )
                          )
                        );
                      },
                    ),

                    TextButton(
                      onPressed: () {
                        _addStepToInstructionsList(index);
                      },
                      child: Text("Add Step")
                    ),

                  ],
                );
              },
            ),

            TextButton(
              onPressed: () {
                setState(() {
                  _addNewInstructionsList();
                });
              },
              child: Text("Add Instruction List")
            ),

          ],
        )
      )
    );
  }

  void _addNewInstructionsList() {
    CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);
    if (provider.recipe.instructions == null) provider.recipe.instructions = [];
    provider.recipe.instructions!.add(Instruction());
    print(provider.recipe.instructions!.length);
  }

  void _addStepToInstructionsList(int instructionsListIndex) {
    setState(() {

      CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);

      if (provider.recipe.instructions == null) return;

      if (provider.recipe.instructions![instructionsListIndex].steps == null) {
        provider.recipe.instructions![instructionsListIndex].steps = [];
      }

      provider.recipe.instructions![instructionsListIndex].steps!.add(Step());

      print(provider.recipe.instructions![instructionsListIndex].steps![0].text.toString());

    });
  }


  @override
  bool get wantKeepAlive => true;
}
