import 'package:flutter/material.dart';
import 'package:recipe_stornado/models/recipe.dart';

class CreateSoloIngredient extends StatelessWidget {

  final int index;
  final Function(int, Ingredient) onSaved;
  final Ingredient? initIngredient;
  final Ingredient ingredient = new Ingredient();

  CreateSoloIngredient({required this.index, required this.onSaved, this.initIngredient});

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Create Ingredient"),
        ),
        body: Column(
          children: [

            TextFormField(
              keyboardType: TextInputType.number,
              initialValue: initIngredient?.amount.toString()??"",
              decoration: InputDecoration(
                labelText: "Amount",
              ),
              validator: (String? value) {
                if (value == null) return null;
                if (double.tryParse(value) == null) return "Enter a valid number";
                return null;
              },
              onSaved: (String? value) {
                if (value == null) return;
                ingredient.amount = double.parse(value);
              },
            ),

            TextFormField(
              initialValue: initIngredient?.unit.toString()??"",
              decoration: InputDecoration(
                labelText: "Unit",
              ),
              onSaved: (String? value) {
                ingredient.unit = value;
              },
            ),

            TextFormField(
              initialValue: initIngredient?.name.toString()??"",
              decoration: InputDecoration(
                labelText: "Name",
              ),
              validator: (String? value) {
                if (value == null) return "Cannot be null";
                if (value.isEmpty) return "Enter something";
                if (value.trim().length == 0) return "Enter something";
                return null;
              },
              onSaved: (String? value) {
                ingredient.name = value;
              },
            ),

            TextFormField(
              initialValue: initIngredient?.comment.toString()??"",
              decoration: InputDecoration(
                labelText: "Comment",
              ),
              onSaved: (String? value) {
                ingredient.comment = value;
              },
            ),

          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              onSaved(index, ingredient);
              Navigator.pop(context);
            }
          }
        ),
      ),
    );
  }
}
