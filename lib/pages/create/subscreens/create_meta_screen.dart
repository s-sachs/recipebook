import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/create/subscreens/create_tags_screen.dart';
import 'package:recipe_stornado/provider/create_provider.dart';

class CreateMetaScreen extends StatefulWidget {
  @override
  _CreateMetaScreenState createState() => _CreateMetaScreenState();
}

class _CreateMetaScreenState extends State<CreateMetaScreen> with AutomaticKeepAliveClientMixin<CreateMetaScreen> {


  final Color? _tfColor = null;//Colors.grey[300];
  final bool _tfFilled = true;

  final double spacing = 10.0;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Column(
            children: [

              TextFormField(
                initialValue: (provider.recipe.title??"").toString(),
                decoration: InputDecoration(
                  icon: Icon(Icons.text_fields),
                  labelText: "Recipe Title",
                  fillColor: _tfColor,
                  filled: _tfFilled,
                ),
                maxLines: null,
                validator: (String? value) {
                  if (value == null) return "Enter a title";
                  if (value.length == 0) return "Enter a title";
                  return null;
                },
                onSaved: (String? value) {
                  provider.recipe.title = value??"";
                },
              ),

              SizedBox(height: spacing),

              TextFormField(
                initialValue: (provider.recipe.description??"").toString(),
                decoration: InputDecoration(
                  icon: Icon(Icons.description),
                  labelText: "Recipe Description",
                  fillColor: _tfColor,
                  filled: _tfFilled,
                ),
                maxLines: null,
                onSaved: (String? value) {
                  provider.recipe.description = value;
                },
              ),

              SizedBox(height: spacing),

              TextFormField(
                initialValue: (provider.recipe.originUrl??"").toString(),
                decoration: InputDecoration(
                  icon: Icon(Icons.description),
                  labelText: "Origin URL",
                  fillColor: _tfColor,
                  filled: _tfFilled,
                ),
                minLines: 1,
                maxLines: 3,
                onSaved: (String? value) {
                  provider.recipe.originUrl = value??"";
                },
              ),

              SizedBox(height: spacing),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Flexible(
                    flex: 1,
                    child: Padding(
                      padding: EdgeInsets.only(right: 5.0),
                      child: TextFormField(
                        initialValue: (provider.recipe.rating??"").toString(),
                        decoration: InputDecoration(
                          labelText: "Rating",
                          fillColor: _tfColor,
                          filled: _tfFilled,
                        ),
                        keyboardType: TextInputType.number,
                        validator: (String? value) {
                          if (value == null) return null;
                          if (double.tryParse(value) == null) return "Enter valid number";
                          if (double.parse(value) < 0 || 5.0 < double.parse(value)) return "Must be between 0.0 and 5.0";
                          return null;
                        },
                        onSaved: (String? value) {
                          //double d = double.parse(value??"0");
                          provider.recipe.rating = double.parse(value??"0");
                        },
                      ),
                    ),
                  ),

                  Flexible(
                    flex: 1,
                    child: Padding(
                      padding: EdgeInsets.only(left: 5.0),
                      child: TextFormField(
                        initialValue: (provider.recipe.difficulty??"").toString(),
                        decoration: InputDecoration(
                          labelText: "Difficulty",
                          fillColor: _tfColor,
                          filled: _tfFilled,
                        ),
                        keyboardType: TextInputType.number,
                        validator: (String? value) {
                          if (value == null) return null;
                          if (double.tryParse(value) == null) return "Enter valid number";
                          return null;
                        },
                        onSaved: (String? value) {
                          //double d = double.parse(value??"0");
                          provider.recipe.difficulty = double.parse(value??"0");
                        },
                      ),
                    ),
                  ),
                ],
              ),

              SizedBox(height: spacing),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    flex: 1,
                    child: TextFormField(
                      initialValue: (provider.recipe.timePrep??"").toString(),
                      decoration: InputDecoration(
                        labelText: "Preparation Time",
                        fillColor: _tfColor,
                        filled: _tfFilled,
                      ),
                      keyboardType: TextInputType.number,
                      validator: (String? value) {
                        if (value == null) return "Enter a number";
                        int? number = int.tryParse(value);
                        if (number == null) return "Enter valid number";
                        if (number <= 0) return "Number must be > 0";
                        return null;
                      },
                      onSaved: (String? value) {
                        provider.recipe.timePrep = int.parse(value??"0");
                      },
                    ),
                  ),

                  Flexible(
                    flex: 1,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextFormField(
                        initialValue: (provider.recipe.timeCook??"").toString(),
                        decoration: InputDecoration(
                          labelText: "Cooking Time",
                          fillColor: _tfColor,
                          filled: _tfFilled,
                        ),
                        keyboardType: TextInputType.number,
                        validator: (String? value) {
                          if (value == null) return "Enter a number";
                          int? number = int.tryParse(value);
                          if (number == null) return "Enter valid number";
                          if (number <= 0) return "Number must be > 0";
                          return null;
                        },
                        onSaved: (String? value) {
                          provider.recipe.timeCook = int.parse(value??"0");
                        },
                      ),
                    ),
                  ),

                  Flexible(
                    flex: 1,
                    child: TextFormField(
                      initialValue: (provider.recipe.timeTotal??"").toString(),
                      decoration: InputDecoration(
                        labelText: "Total Time",
                        fillColor: _tfColor,
                        filled: _tfFilled,
                      ),
                      keyboardType: TextInputType.number,
                      validator: (String? value) {
                        if (value == null) return null;
                        int? number = int.tryParse(value);
                        if (number == null) return "Enter valid number";
                        if (number <= 0) return "Number must be > 0";
                        return null;
                      },
                      onSaved: (String? value) {
                        provider.recipe.timeTotal = int.parse(value??"0");
                      },
                    ),
                  ),


                ],
              ),

              SizedBox(height: spacing),


              TextFormField(
                initialValue: provider.recipe.picture != null ? (provider.recipe.picture!.url??"").toString() : "",
                decoration: InputDecoration(
                  icon: Icon(Icons.description),
                  labelText: "Image URL",
                  fillColor: _tfColor,
                  filled: _tfFilled,
                ),
                minLines: 1,
                maxLines: 3,
                onSaved: (String? value) async {
                  provider.recipe.picture == null
                    ? provider.recipe.picture = RecipePicture(url: value??"")
                    : provider.recipe.picture!.url = value??"";
                },
              ),

              TextButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CreateTagsScreen(provider: provider)));
                },
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    //border: Border(bottom: BorderSide(width: 1, color: Colors.grey)),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.grey.shade300,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ImageIcon(AssetImage("assets/icons/icons8-tag-window-50.png")),
                      SizedBox(width: 5.0),
                      Text("Choose tags"),
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
