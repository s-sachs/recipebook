import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/pages/create/create_recipe_screen.dart';
import 'package:recipe_stornado/pages/create/widgets/primary_button.dart';
import 'package:recipe_stornado/pages/details/recipe_details.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';

class CreateChoiceScreen extends StatefulWidget {
  @override
  CreateChoiceScreenState createState() => CreateChoiceScreenState();
}

class CreateChoiceScreenState extends State<CreateChoiceScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          PrimaryButton(text: "Create Recipe", press: _createRecipeButton),
          PrimaryButton(text: "Create Recipe", press: _importRecipeButton, color: Colors.orange),
        ],
      )
    );
  }

  void _createRecipeButton() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CreateRecipeScreen()));
  }

  _importRecipeButton() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => RecipeDetailsScreen(recipe: Provider.of<RecipeProvider>(context, listen: false).dummyRecipe())));
  }

}
