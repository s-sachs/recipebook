import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {

  final String text;
  late final Color color;
  final EdgeInsets padding;
  final VoidCallback press;

  PrimaryButton({
    required this.text,
    required this.press,
    this.color = Colors.green,
    this.padding = const EdgeInsets.all(20 * 0.75),
  });


  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      padding: padding,
      color: color,
      minWidth: double.infinity,
      onPressed: press,
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
      )
    );
  }

}
