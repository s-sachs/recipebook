import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/pages/create/subscreens/create_ingredients_screen.dart';
import 'package:recipe_stornado/pages/create/subscreens/create_instructions_screen.dart';
import 'package:recipe_stornado/pages/create/subscreens/create_meta_screen.dart';
import 'package:recipe_stornado/provider/create_provider.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';
import 'package:recipe_stornado/provider/user_provider.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

class CreateRecipeScreen extends StatefulWidget {

  final Recipe? recipe;

  const CreateRecipeScreen({Key? key, this.recipe}) : super(key: key);

  @override
  _CreateRecipeScreenState createState() => _CreateRecipeScreenState();
}

class _CreateRecipeScreenState extends State<CreateRecipeScreen> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: DefaultTabController(
        length: 3,
        child: ChangeNotifierProvider(
          create: (context) => widget.recipe == null ? CreateProvider() : CreateProvider.withRecipe(widget.recipe!),
          child: Scaffold(
            body: NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget> [
                  new SliverAppBar(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("CREATE"),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: Colors.teal),
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              _formKey.currentState!.save();

                              Recipe recipe = Provider.of<CreateProvider>(context, listen: false).recipe;

                              if(recipe.uuid == null) recipe.uuid = Uuid().v1();
                              recipe.ownerId = Provider.of<UserProvider>(context, listen: false).userId;
                              recipe.createdAt = DateTime.now().toString();
                              recipe.updatedAt = recipe.createdAt;

                              //recipe.tags = ["dev, create"];
                              recipe.nutrition = [];

                              recipe.version = Provider.of<UserProvider>(context, listen: false).version;


                              recipe.picture = await getRecipePicture(recipe.picture!.url??"");


                              if (recipe.id != null) {
                                await Provider.of<RecipeProvider>(context, listen: false).updateRecipe(recipe);
                              } else {
                                await Provider.of<RecipeProvider>(context, listen: false).addRecipe(recipe);
                              }

                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Added new Recipe")));

                              Navigator.pop(context);
                            }
                          },//_onSavePressed,
                          child: Text("Add"),
                        ),
                      ],
                    ),
                    pinned: true,
                    floating: true,
                    bottom: TabBar(
                      tabs: [
                        Tab(text: "Meta"),
                        Tab(text: "Ingredients"),
                        Tab(text: "Instructions"),
                      ],
                    ),
                  )
                ];
              },
              body:  TabBarView(
                children: [
                  CreateMetaScreen(),
                  CreateIngredientsScreen(),
                  CreateInstructionsScreen(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSavePressed() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("SUBMIT")));

      Recipe recipe = Provider.of<CreateProvider>(context, listen: false).recipe;
      //Provider.of<RecipeProvider>(context, listen: false).addRecipe(recipe);

      Navigator.pop(context);
    }
  }

  Future<RecipePicture?> getRecipePicture(String url) async {
    try {
      print(url);
      http.Response response = await http.get(Uri.parse(url));
      RecipePicture picture = RecipePicture(
        title: "",
        url: url,
        picture: response.bodyBytes,
      );
      return picture;
    } catch (error) {
      print(error);
      return null;
    }
  }

}
