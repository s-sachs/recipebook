import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/pages/home.dart';
import 'package:recipe_stornado/pages/home/home_page.dart';
import 'package:recipe_stornado/pages/new/new_page.dart';
import 'package:recipe_stornado/provider/recipe_provider.dart';
import 'package:recipe_stornado/provider/user_provider.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  late RecipeProvider recipeProvider;

  @override
  void initState() {
    this.recipeProvider = RecipeProvider();
    recipeProvider.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => recipeProvider),
        ChangeNotifierProvider(create: (context) => UserProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Recipe Stornado",
        theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.green,
          ).copyWith(
            secondary: Colors.red,
          ),

          primaryColor: Colors.green,
          accentColor: Colors.green.shade800,

          
          //backgroundColor: Color.fromARGB(255, 255, 247, 227),
          //scaffoldBackgroundColor: Colors.green.shade50,//Color.fromARGB(255, 255, 247, 227),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
            backgroundColor: Colors.green.shade100,
          ),

          //primaryColor: Color.fromARGB(255, 255, 247, 227),
          //backgroundColor: Color.fromARGB(255, 183, 110, 121),


        ),
        //home: HomePage(),
        //home: NewPage(),
        home: Home(),
      ),
    );
  }
}
