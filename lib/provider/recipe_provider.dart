import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart' hide Step;
import 'package:path/path.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/util/filter/recipe_filters.dart';
import 'package:recipe_stornado/util/placeholder_recipes.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';


class RecipeProvider extends ChangeNotifier {

  late RecipeDBHandler dbHandler;

  List<Recipe> recipes = [];

  List<String> favorites = [];

  bool initialized = false;

  // TODO: Add support for language parameter in recipe class
  // add to parsing and database

  void init() async {
    dbHandler = new RecipeDBHandler();
    await dbHandler.initDatabase();
    notifyListeners();
  }

  Future<void> refreshRecipes() async {
    print("REFRESH");
    recipes = await dbHandler.getRecipes();
    //recipes = PlaceholderRecipes.list;
    if (!initialized) initialized = true;
    notifyListeners();
  }

  Future<List<Recipe>> getRecipes() async {
    //if (recipes.length == 0) await refreshRecipes();
    if (!initialized) await refreshRecipes();
    return recipes;
  }

  Future<void> toggleFavorite(Recipe recipe) async {
    refreshFavorites();
    if (recipe.uuid != null) {
      if (favorites.contains(recipe.uuid!)) {
        favorites.removeWhere((e) => recipe.uuid! == e);
        await dbHandler.deleteFavorite(recipe.uuid!);
      } else {
        favorites.add(recipe.uuid!);
        await dbHandler.insertFavorite(recipe.uuid!);
      }
    }
    notifyListeners();
  }

  Future<void> refreshFavorites() async {
    favorites = await dbHandler.getFavorites();
    notifyListeners();
  }

  Future<List<Recipe>> getFavoriteRecipes() async {
    if (favorites.length == 0) await refreshFavorites();
    return (await getRecipes()).where((e) => isFavorite(e)).toList();
  }

  bool isFavorite(Recipe recipe) {
    return favorites.contains(recipe.uuid);
  }

  Future<bool> isFav(Recipe recipe) async {
    await refreshFavorites();
    return favorites.contains(recipe.uuid);
  }


  Future<Recipe?> addRecipe(Recipe recipe) async {
    refreshRecipes();
    return await dbHandler.insertRecipe(recipe);
  }

  Future<Recipe?> updateRecipe(Recipe recipe) async {
    refreshRecipes();
    return await dbHandler.updateRecipe(recipe);
  }

  Future<int> removeRecipe(Recipe recipe) async {
    refreshRecipes();
    return await dbHandler.deleteRecipe(recipe);
  }

  Recipe dummyRecipe() {
    Recipe recipe = new Recipe();

    recipe.uuid = Uuid().v1();
    recipe.ownerId = "Person#1";

    recipe.title = "Dummy Recipe";
    recipe.description = "This is a dummy recipe which is used for development purposes.";
    recipe.originUrl = "local";

    recipe.rating = 3.7;
    recipe.difficulty = 1;

    recipe.timePrep = 10;
    recipe.timeCook = 20;
    recipe.timeTotal = 30;

    recipe.servings = 2;

    recipe.ingredientsSubLists = [
      IngredientsSubList(
        title: "For the chicken",
        ingredients: [
          Ingredient(
            amount: 300.0,
            unit: "g",
            name: "cream",
          ),
          Ingredient(
            amount: 250.0,
            unit: "g",
            name: "chicken",
          ),
          Ingredient(
            name: "salt",
          ),
        ],
      )
    ];

    recipe.instructions = [
      Instruction(
        title: "Prepare the chicken",
        steps: [
          Step(
            number: 1,
            text: "Sear the chicken"
          ),
          Step(
            number: 2,
            text: "Add the cream"
          ),
          Step(
            number: 3,
            text: "Enjoy"
          ),
        ],
      ),
    ];


    recipe.createdAt = DateTime.now().toString();
    recipe.updatedAt = recipe.createdAt;

    recipe.nutrition = ["Alice", "Charlie", "Emma"];
    recipe.tags = ["tick", "trick", "track"];

    recipe.version = "0.0.1";

    return recipe;
  }

}

class RecipeDBHandler {

  final String favoritesTable = "favorites";

  final String recipesTable = "recipes";
  final String instructionsTable = "instructions";
  final String stepsTable = "step";
  final String ingredientsSubListsTable = "ingredients_sub_lists";
  final String ingredientsTable = "ingredients";
  final String recipePicturesTable = "recipe_pictures";

  late Future<Database> database;

  RecipeDBHandler() {
    //initDatabase();
  }

  Future<bool> initDatabase() async {
    database = openDatabase(
      join(await getDatabasesPath(), 'recipe_database.db'),
      onCreate: (db, version) {

        db.execute(
          "CREATE TABLE $favoritesTable(id INTEGER PRIMARY KEY AUTOINCREMENT, recipe_uuid TEXT)"
        );

        db.execute(
          "CREATE TABLE $recipesTable(id INTEGER PRIMARY KEY AUTOINCREMENT, recipe_uuid TEXT, owner_uuid TEXT,"+
            "title TEXT, description TEXT, origin_url TEXT, rating REAL, difficulty REAL,"+
            "time_prep INTEGER, time_cook INTEGER, time_total INTEGER, servings INTEGER,"+
            "tags TEXT, nutrition TEXT, created_at TEXT, updated_at TEXT, language TEXT, version TEXT)"
        );

        db.execute(
          "CREATE TABLE $instructionsTable(id INTEGER PRIMARY KEY AUTOINCREMENT, recipe_id INTEGER,"+
            "title TEXT, comment TEXT)"
        );
        db.execute(
          "CREATE TABLE $stepsTable(id INTEGER PRIMARY KEY AUTOINCREMENT, instruction_id INTEGER,"+
            "recipe_id INTEGER, number INTEGER, text TEXT, comment TEXT)"
        );

        db.execute(
          "CREATE TABLE $ingredientsSubListsTable(id INTEGER PRIMARY KEY AUTOINCREMENT, recipe_id INTEGER,"+
            "title TEXT)"
        );
        db.execute(
          "CREATE TABLE $recipePicturesTable(id INTEGER PRIMARY KEY AUTOINCREMENT, recipe_id INTEGER," +
            "title TEXT, url TEXT, picture BLOB)"
        );
        return db.execute(
          "CREATE TABLE $ingredientsTable(id INTEGER PRIMARY KEY AUTOINCREMENT, sublist_id INTEGER,"+
            "recipe_id INTEGER, amount REAL, unit TEXT, name TEXT, comment TEXT)"
        );

      },
      version: 1,
    );
    return true;
  }

  Future<List<String>> getFavorites() async {
    final db = await database;

    List<String> favorites = [];
    final List<Map<String, dynamic>> favoriteMaps = await db.query(favoritesTable);
    for (Map<String, dynamic> map in favoriteMaps) {
      favorites.add(map['recipe_uuid']);
    }

    return favorites;
  }

  Future<int> deleteFavorite(String uuid) async {
    final db = await database;
    return await db.delete(
      favoritesTable,
      where: 'recipe_uuid = ?',
      whereArgs: [uuid],
    );
  }

  Future<int> insertFavorite(String uuid) async {
    final db = await database;
    return await db.insert(
      favoritesTable,
      {'recipe_uuid': uuid},
    );
  }

  Future<List<Recipe>> getRecipes() async {
    final db = await database;

    // TODO: outsource setters into a mapToRecipe(Map<x,y>) method
    // only create the map here and call the method at the end

    // get all Recipes
    final List<Map<String, dynamic>> recipeMaps = await db.query(recipesTable);

    List<Recipe> recipeList = [];

    try {

      // iterate through all recipe maps
      for (Map<String, dynamic> recipeMap in recipeMaps) {
        Recipe recipe = Recipe();
        recipe.id = recipeMap['id'];
        recipe.uuid = recipeMap['recipe_uuid'];
        recipe.ownerId = recipeMap['owner_uuid'];
        recipe.title = recipeMap['title'];
        recipe.description = recipeMap['description'];
        recipe.originUrl = recipeMap['origin_url'];
        recipe.rating = recipeMap['rating'];
        recipe.difficulty = recipeMap['difficulty'];
        recipe.timePrep = recipeMap['time_prep'];
        recipe.timeCook = recipeMap['time_cook'];
        recipe.timeTotal = recipeMap['time_total'];
        recipe.servings = recipeMap['servings'];
        recipe.tags = (recipeMap['tags'] as String).split(",");
        recipe.nutrition = (recipeMap['nutrition'] as String).split(",");
        recipe.createdAt = recipeMap['created_at'];
        recipe.updatedAt = recipeMap['updated_at'];
        recipe.language = recipeMap['language'];
        recipe.version = recipeMap['version'];


        recipe.instructions = [];
        List<Map<String, dynamic>> instructionsMaps = await db.query(
          instructionsTable,
          where: 'recipe_id = ?',
          whereArgs: [recipeMap['id']],
        );

        for (Map<String, dynamic> instructionMap in instructionsMaps) {
          Instruction instruction = Instruction();
          instruction.title = instructionMap['title'];
          instruction.comment = instructionMap['comment'];
          instruction.steps = [];

          List<Map<String, dynamic>> stepsMaps = await db.query(
            stepsTable,
            where: 'instruction_id = ?',
            whereArgs: [instructionMap['id']],
          );

          for (Map<String, dynamic> stepMap in stepsMaps) {
            Step step = Step();
            step.number = stepMap['number'];
            step.text = stepMap['text'];
            step.comment = stepMap['comment'];
            instruction.steps!.add(step);
          }

          recipe.instructions!.add(instruction);
        }

        recipe.ingredientsSubLists = [];
        List<Map<String, dynamic>> ingredientsSubListsMaps = await db.query(
          ingredientsSubListsTable,
          where: 'recipe_id = ?',
          whereArgs: [recipeMap['id']],
        );

        for (Map<String, dynamic> subListMap in ingredientsSubListsMaps) {
          IngredientsSubList subList = IngredientsSubList();
          subList.title = subListMap['title'];
          subList.ingredients = [];

          List<Map<String, dynamic>> ingredientsMaps = await db.query(
            ingredientsTable,
            where: 'sublist_id = ?',
            whereArgs: [subListMap['id']],
          );

          for (Map<String, dynamic> ingredientMap in ingredientsMaps) {
            Ingredient ingredient = Ingredient();
            ingredient.amount = ingredientMap['amount'];
            ingredient.unit = ingredientMap['unit'];
            ingredient.name = ingredientMap['name'];
            ingredient.comment = ingredientMap['comment'];
            subList.ingredients!.add(ingredient);
          }

          recipe.ingredientsSubLists!.add(subList);
        }

        List<Map<String, dynamic>> pictureMapList = await db.query(
          recipePicturesTable,
          where: 'recipe_id = ?',
          whereArgs: [recipeMap['id']],
        );

        if (pictureMapList.length > 0) {
          recipe.picture = RecipePicture.fromMap(pictureMapList.first);
        }

        recipeList.add(recipe);
      }

      print(recipeList);

      return recipeList;
    } catch (error) {
      print(error);
      print("ERROR GETTING RECIPES");
      throw error;
    }
  }

  /// Deletes the given recipe from the database
  /// It first tries to delete the recipe from the recipes table.
  /// If that succeeded, it looks for references of this recipe in the other tables.
  Future<int> deleteRecipe(Recipe recipe) async {
    final db = await database;
    if (recipe.id == null) return -1;

    try {

      int recipeStatus = await db.delete(
        recipesTable,
        where: 'id = ?',
        whereArgs: [recipe.id],
      );

      if (recipeStatus > 0) {

        int instructionsStatus = await db.delete(
          instructionsTable,
          where: 'recipe_id = ?',
          whereArgs: [recipe.id],
        );

        if (instructionsStatus > 0) {
          await db.delete(
            stepsTable,
            where: 'recipe_id = ?',
            whereArgs: [recipe.id],
          );
        }

        int ingredientsSubListStatus = await db.delete(
          ingredientsSubListsTable,
          where: 'recipe_id = ?',
          whereArgs: [recipe.id],
        );

        if (ingredientsSubListStatus > 0) {
          await db.delete(
            ingredientsTable,
            where: 'recipe_id = ?',
            whereArgs: [recipe.id],
          );
        }

        int pictureStatus = await db.delete(
          recipePicturesTable,
          where: 'recipe_id = ?',
          whereArgs: [recipe.id],
        );
      }

      print("DELETED $recipeStatus entries from '$recipesTable'");

      return recipeStatus;

    } catch (error) {
      print("ERROR_DEL: $error");
      return -1;
    }
  }


  Future<Recipe?> insertRecipe(Recipe recipe) async {
    final db = await database;

    int? recipeId = -1;

    try {

      recipeId = await db.insert(
        recipesTable,
        recipe.toMap(),
      );

      recipe.id = recipeId;

      for (Instruction instruction in recipe.instructions??[]) {
        var instructionMap = instruction.toMap();
        instructionMap['recipe_id'] = recipeId;
        int instructionId = await db.insert(
          instructionsTable,
          instructionMap,
        );

        for (Step step in instruction.steps??[]) {
          var stepMap = step.toMap();
          stepMap['recipe_id'] = recipeId;
          stepMap['instruction_id'] = instructionId;
          db.insert(
            stepsTable,
            stepMap,
          );
        }
      }

      for (IngredientsSubList sublist in recipe.ingredientsSubLists??[]) {
        var sublistMap = sublist.toMap();
        sublistMap['recipe_id'] = recipeId;
        int sublistId = await db.insert(
          ingredientsSubListsTable,
          sublistMap,
        );

        for (Ingredient ingredient in sublist.ingredients??[]) {
          var ingredientMap = ingredient.toMap();
          ingredientMap['recipe_id'] = recipeId;
          ingredientMap['sublist_id'] = sublistId;
          db.insert(
            ingredientsTable,
            ingredientMap,
          );
        }
      }

      if (recipe.picture != null) {
        var pictureMap = recipe.picture!.toMap();
        pictureMap['recipe_id'] = recipeId;
        int pictureId = await db.insert(
          recipePicturesTable,
          pictureMap,
        );
        if (recipe.picture!.id == null) recipe.picture!.id = pictureId;
      }

    } on DatabaseException catch (error) {
      throw (error);
    } on DatabaseHelperException catch (error) {
      throw (error);
    } catch (error) {
      throw (error);
    }

    if (recipeId < 0) return null;

    return recipe;
  }

  Future<Recipe?> updateRecipe(Recipe recipe) async {
    try {
      await deleteRecipe(recipe);
      return await insertRecipe(recipe);
    } catch (error) {
      print(error);
      return null;
    }
  }


  /// Helper class to create a map out of a recipe
  Map<String, dynamic> recipeToMap(Recipe recipe) {
    Map<String, dynamic> recipeMap = Map();

    //recipeMap['id'] = recipe.id;
    recipeMap['recipe_uuid'] = recipe.uuid;
    recipeMap['owner_uuid'] = recipe.ownerId;
    recipeMap['title'] = recipe.title;
    recipeMap['description'] = recipe.description;
    recipeMap['origin_url'] = recipe.originUrl;
    recipeMap['rating'] = recipe.rating;
    recipeMap['difficulty'] = recipe.difficulty;
    recipeMap['time_prep'] = recipe.timePrep;
    recipeMap['time_cook'] = recipe.timeCook;
    recipeMap['time_total'] = recipe.timeTotal;
    recipeMap['servings'] = recipe.servings;
    recipeMap['tags'] = (recipe.tags??[]).join(",");
    recipeMap['nutrition'] = (recipe.nutrition??[]).join(",");
    recipeMap['created_at'] = recipe.createdAt;
    recipeMap['updated_at'] = recipe.updatedAt;
    recipeMap['language'] = recipe.language;
    recipeMap['version'] = recipe.version;

    List<Map<String, dynamic>> instructionMapList = [];
    for (Instruction instruction in recipe.instructions??[]) {
      Map<String, dynamic> instructionMap = Map();
      instructionMap['title'] = instruction.title;
      instructionMap['comment'] = instruction.comment;

      List<Map<String, dynamic>> stepsMapList = [];
      for (Step step in instruction.steps??[]) {
        Map<String, dynamic> stepMap = Map();
        stepMap['number'] = step.number;
        stepMap['text'] = step.text;
        stepMap['comment'] = step.comment;
        stepsMapList.add(stepMap);
      }
      instructionMap['steps'] = stepsMapList;
      instructionMapList.add(instructionMap);
    }
    recipeMap['instructions'] = instructionMapList;

    List<Map<String, dynamic>> ingredientsSubListMapList = [];
    for (IngredientsSubList sublist in recipe.ingredientsSubLists??[]) {
      Map<String, dynamic> sublistMap = Map();
      sublistMap['title'] = sublist.title;

      List<Map<String, dynamic>> ingredientsMapsList = [];
      for (Ingredient ingredient in sublist.ingredients??[]) {
        Map<String, dynamic> ingredientMap = Map();
        ingredientMap['amount'] = ingredient.amount;
        ingredientMap['unit'] = ingredient.unit;
        ingredientMap['name'] = ingredient.name;
        ingredientMap['comment'] = ingredient.comment;
        ingredientsMapsList.add(ingredientMap);
      }
      sublistMap['ingredients'] = ingredientsMapsList;
      ingredientsSubListMapList.add(sublistMap);
    }
    recipeMap['ingredientSubLists'] = ingredientsSubListMapList;

    if (recipe.picture != null) recipeMap['picture'] = recipe.picture!.toMap();

    printMap(recipeMap);

    return recipeMap;
  }

  /// recursively prints every key/value pair in the given map and its children
  void printMap(Map map) {
    for (String key in map.keys) {
      if (map[key] is List) {
        print(key.toString());
        for (Map m in map[key]) {
          printMap(m);
        }
      } else {
        print(key.toString() + " - " + map[key].toString());
      }
    }
  }

}

class DatabaseHelperException implements Exception {
  String message;
  DatabaseHelperException(this.message);
}