import 'package:flutter/material.dart';
import 'package:recipe_stornado/models/recipe.dart';

class CreateProvider extends ChangeNotifier {

  late Recipe recipe;

  CreateProvider() {
    this.recipe = new Recipe();
  }

  CreateProvider.withRecipe(Recipe recipe) {
    this.recipe = recipe;
  }

}