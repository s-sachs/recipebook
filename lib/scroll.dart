import 'package:flutter/material.dart';

class MenuList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MenuListState();
  }



}

class _MenuListState extends State<MenuList> {
  static const double _appBarBottomBtnPosition = 24.0;
  //24.0; //change this value to position your button vertically

  List<String> items = List.generate(45, (e) => e.toString());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text(
              'Testing',
              style: TextStyle(color: Colors.red),
            ),
          ),
          SliverAppBar(
            pinned: true,
            expandedHeight: 300.0,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              titlePadding: EdgeInsets.only(bottom: 25),
              title: Text('Title'),
              background: Image.asset(
                'assets/images/penguin.jpg',
                fit: BoxFit.cover,
              ),
            ),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(0.0),
              child: Transform.translate(
                offset: const Offset(0, _appBarBottomBtnPosition),
                child: RaisedButton(
                  shape: StadiumBorder(),
                  child: Text("Click Here"),
                  onPressed: () {},
                ),
              ),
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.only(top: _appBarBottomBtnPosition),
          ),
          SliverGrid.count(
            crossAxisCount: 2,
            childAspectRatio: 2/3,
            children: transformToWidgets(items),
          ),
        ],
      ),
    );
  }

  List<Widget> transformToWidgets(List<String> strings) {
    return strings.map((e) => Container(child: Center(child: Text(e.toString())), color: Colors.lightGreen)).toList();
  }

}