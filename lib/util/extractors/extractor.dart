import 'dart:io';

import 'package:html/parser.dart';
import 'package:http/http.dart' as http;
import 'package:recipe_stornado/models/recipe.dart';
import 'package:uuid/uuid.dart';

void main(List<String> args) async {
  String url = "https://www.chefkoch.de/rezepte/376861123780998/Suesskartoffel-Auflauf.html";
  url =
  "https://www.chefkoch.de/rezepte/1575521265121057/Zucchini-Kuechlein-mit-Joghurtdip.html";
  Extractor.fetchRecipeFromUrl(url);
}

class Extractor {

  /// Fetches the html data from the given uri.
  static Future<Recipe?> fetchRecipeFromUrl(String uri) async {
    var url = Uri.parse(uri);
    var response = await http.get(url);

    if (response.statusCode == 200) {
      Recipe recipe = await extractRecipeFromHtml(uri, response.body);

      return recipe;
    } else {
      print("Request failed with status: ${response.statusCode}");
      return null;
    }
  }

  /// Extracts data for a chefkoch recipe html.
  static Future<Recipe> extractRecipeFromHtml(String url, String data) async {
    if (data == "") data = await readFile();
    var document = parse(data);

    Recipe recipe = new Recipe();

    // uuid
    var uuid = Uuid();
    recipe.uuid = uuid.v1();

    var ownerId = "ChefkochExtract";

    // title
    print("TITLE:");
    var title = document.body!.getElementsByClassName("recipe-header")[0]
        .children[0].children[0];
    print(title.text);
    recipe.title = title.text.trim();

    // description
    print("DESCRIPTION:");
    var descriptionElements = document.body!.getElementsByClassName(
        "recipe-text");
    if (descriptionElements.length > 0) {
      String description = descriptionElements[0].text.trim();
      print(description);
      recipe.description = description;
    }


    // originUrl
    print("ORIGIN_URL:");
    print(url);
    recipe.originUrl = url;


    // picture
    print("IMAGE_URL:");
    var imgUrl = document.body!.getElementsByClassName(
        "recipe-image-carousel")[0].getElementsByTagName("img")[0]
        .attributes["src"];
    print(imgUrl);
    recipe.picture = RecipePicture(url: imgUrl??"");


    // rating
    print("RATING ORIGINAL:");
    var ratingOriginal = document.body!.getElementsByClassName(
        "ds-rating-avg")[0].children[0].children[1];
    print(ratingOriginal.text.trim());
    double? ratingOriginalDouble = double.tryParse(ratingOriginal.text.trim());
    recipe.rating = ratingOriginalDouble??0.0;


    // times
    var times = document.body!.getElementsByClassName("rds-recipe-meta")[0];
    int totalIndex = times.children.length - 1;

    RegExp regExp = new RegExp(r".\d.");

    print("TIME PREPARATION:");
    var timePrep = times.children[0];
    print(timePrep.text);
    recipe.timePrep = convertStringToInt(
        regExp.firstMatch(timePrep.text)!.group(0).toString().trim());

    if (totalIndex == 2) {
      print("TIME COOKING:");
      var timeCook = times.children[1];
      print(timeCook.text);
      recipe.timeCook = convertStringToInt(
          regExp.firstMatch(timeCook.text)!.group(0).toString().trim());
    } else {
      recipe.timeCook = 0;
    }

    print("TIME TOTAL:");
    var timeTotal = times.children[totalIndex];
    print(timeTotal.text);
    recipe.timeTotal = convertStringToInt(
        regExp.firstMatch(timeTotal.text)!.group(0).toString().trim());

    // difficulty
    print("DIFFICULTY:");
    var difficulty = document.body!.getElementsByClassName(
        "recipe-difficulty")[0];
    print(difficulty.text);
    recipe.difficulty = 1;

    // servings
    print("SERVINGS:");
    var servings = document.body!.getElementsByClassName("recipe-servings")[0]
        .children[0].children[0].attributes["value"];
    print(servings);
    recipe.servings = convertStringToInt(servings??"4");


    // ingredients
    print("INGREDIENTS:");
    var ingredientTables = document.body!.getElementsByClassName("ingredients");
    List<IngredientsSubList> ingredientsSubListList = [];
    for (var table in ingredientTables) {
      IngredientsSubList ingredientsSubList = new IngredientsSubList();
      // list title
      var tableHead = table.getElementsByTagName("thead");
      if (tableHead.isNotEmpty) {
        String title = tableHead[0].text.trim();
        print(title);
        ingredientsSubList.title = title;
      }

      // list body
      var tableBodies = table.getElementsByTagName("tbody");
      List<Ingredient> ingredients = [];
      for (var tableBody in tableBodies) {
        // ingredient rows
        var rows = tableBody.children;
        for (var row in rows) {
          Ingredient ingredient = new Ingredient();
          String amountString = row.children[0].text.trim();
          String ingredientName = row.children[1].text.trim();
          print("amount    : " + amountString);
          print("ingredient: " + ingredientName);

          List<String> amountSplit = [];
          if (amountString.isNotEmpty) {
            String s = amountString;
            print(s);
            int idx = s.indexOf(" ");
            if (idx >= 0) {
              amountSplit = [
                s.substring(0, idx),
                s.substring(idx + 1)
              ];
            }
          }

          ingredient.amount = amountSplit.length > 0 ? convertStringToDouble(amountSplit.first.trim()) : 0.0;
          ingredient.unit = amountSplit.length > 1 ? amountSplit.last.trim() : "";
          int idx = ingredientName.indexOf(":");
          if (idx > 0) {
            List parts = [
              ingredientName.substring(0, idx).trim(),
              ingredientName.substring(idx + 1).trim()
            ];
            ingredient.name = parts.first;
            if (parts.length > 1) {
              ingredient.comment = parts.last;
            }
          } else {
            ingredient.name = ingredientName;
          }

          ingredients.add(ingredient);
        }
      }
      ingredientsSubList.ingredients = ingredients;
      ingredientsSubListList.add(ingredientsSubList);
    }
    recipe.ingredientsSubLists = ingredientsSubListList;

    // instructions
    // TODO: rework instructions extraction
    print("INSTRUCTIONS:");
    var elements = document.body!.getElementsByClassName("ds-box");
    List<Instruction> instructions = [];
    for (var element in elements) {
      if (element.hasChildNodes()) {
        if (element.children[0].text.contains("Zubereitung")) {
          if (element.children.length > 2) {
            String text = element.children[2].text;
            print("STEP: " + text);
            Instruction instruction = Instruction(
                steps: [Step(number: instructions.length, text: text)]
            );
            instructions.add(instruction);
          }
        }
      }
    }
    recipe.instructions = instructions;

    // tags
    print("TAGS:");
    recipe.tags = [];
    var tagElements = document.body!.getElementsByClassName("recipe-tags");
    if (tagElements.length > 0 && tagElements[0].children.length > 0) {
      var tags = tagElements[0].children[0].children;
      for (var tag in tags) {
        print(tag.text.trim());
        recipe.tags!.add(tag.text.trim());
      }
    }

    // nutrition
    print("NUTRITION:");
    recipe.nutrition = [];
    var nutritionElements = document.body!.getElementsByClassName(
        "recipe-nutrition_content");
    if (nutritionElements.length > 0) {
      var nutritionList = nutritionElements[0].children;
      for (var nutrition in nutritionList) {
        print(nutrition.text.trim());
        recipe.nutrition!.add(nutrition.text.trim());
      }
    }

    // createdAt
    print("CREATED_AT:");
    String date = DateTime.now().toString();
    print(date);
    recipe.createdAt = date;

    // updatedAt
    print("UPDATED_AT:");
    print(date);
    recipe.updatedAt = date;

    // language
    print("LANGUAGE:");
    recipe.language = "DE";

    // version
    print("VERSION:");
    recipe.version = "0.0.1";

    return recipe;
  }

  /// Tries to parse the given string into an integer.
  /// If the parsing fails, it returns 0 instead.
  static int convertStringToInt(String text) {
    int? i = int.tryParse(text);
    return i != null ? i : 0;
  }

  static double convertStringToDouble(String text) {
    double? d = double.tryParse(text);
    return d != null ? d : 0.0;
  }

  /// Reads a html file. Used for testing the parsing of a recipe without html request.
  static Future<String> readFile() async {
    return await File("./assets/chefkoch.html").readAsString();
  }

  Future<RecipePicture?> getRecipePicture(String url) async {
    try {
      print(url);
      http.Response response = await http.get(Uri.parse(url));
      RecipePicture picture = RecipePicture(
        title: "",
        url: url,
        picture: response.bodyBytes,
      );
      return picture;
    } catch (error) {
      print(error);
      return null;
    }
  }

}

