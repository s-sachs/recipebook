import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_stornado/models/recipe.dart';
import 'package:recipe_stornado/provider/create_provider.dart';

class KEEP extends StatefulWidget {
  @override
  _KEEP createState() => _KEEP();
}

class _KEEP extends State<KEEP> with AutomaticKeepAliveClientMixin<KEEP> {

  List<List<Ingredient>> list = [];

  @override
  Widget build(BuildContext context) {
    super.build(context);

    CreateProvider provider = Provider.of<CreateProvider>(context);

    return Container(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.green,
            onPressed: () {
              initList();
            },
          ),
          body: ListView(
            children: [

              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                //itemCount: list.length,
                itemCount: (provider.recipe.ingredientsSubLists??[]).length,
                itemBuilder: (context, index) {

                  return ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [

                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Title"
                          ),
                        ),
                      ),

                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: list[index].length,
                        itemBuilder: (context, outdex) {
                          Ingredient item = list[index][outdex];
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Flexible(
                                flex: 1,
                                child: Text(item.amount.toString(), textAlign: TextAlign.start,),
                              ),
                              Flexible(
                                flex: 1,
                                child: Text(item.unit.toString()),
                              ),
                              Flexible(
                                flex: 3,
                                child: Text(item.name.toString() + ", " + item.comment.toString()),
                              ),
                            ],
                          );
                        },
                      ),

                      TextButton(onPressed: () {}, child: Text("Add Ingredient")),

                    ],
                  );


                },
              ),

              TextButton(onPressed: () {}, child: Text("Add List")),

            ],
          ),
        )
    );
  }

  void initList() {
    setState(() {
      list = [];
      for(int i = 0; i < 3; i++) {
        list.add([]);
        for(int j = 0; j < 20; j++) {
          Ingredient item = Ingredient(
            amount: 0.0,
            unit: "KG",
            name: "Banana",
            comment: "peeled",
          );
          list[i].add(item);
        }
      }
    });
  }

  void addNewSubList() {
    CreateProvider provider = Provider.of<CreateProvider>(context, listen: false);
    if (provider.recipe.ingredientsSubLists == null) provider.recipe.ingredientsSubLists = [];
    provider.recipe.ingredientsSubLists!.add(IngredientsSubList());
  }



  @override
  bool get wantKeepAlive => true;
}
