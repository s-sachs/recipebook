class TimeHelper {

  static String minutesToFormattedString(int? minutes) {
    String result = "NaN";

    if (minutes == null) return result;

    String hString = "";
    int h = minutes ~/ 60;
    if (h > 1) {
      hString = h.toString() + " hours";
    } else if (h == 1) {
      hString = h.toString() + " hour";
    }

    String minString = "";
    int min = minutes % 60;
    if (min > 1) {
      minString = min.toString() + " minutes";
    } else if (min == 1) {
      minString = min.toString() + " minute";
    }

    if (hString.length > 0 && minString.length > 0) {
      result = hString + ", " + minString;
    } else if (hString.length > 0) {
      result = hString;
    } else if (minString.length > 0) {
      result = minString;
    }

    return result;
  }


}