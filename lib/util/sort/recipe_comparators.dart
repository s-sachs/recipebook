import 'package:recipe_stornado/models/recipe.dart';

class RecipeComparators {

  static final Comparator<Recipe> nameComparator = (a, b) =>
      a.title.toString().compareTo(b.title.toString());

  static final Comparator<Recipe> idComparator = (a, b) =>
      (a.id ?? 0).compareTo(b.id ?? 0);

  static final Comparator<Recipe> createdComparator = (a, b) =>
      DateTime.parse(b.createdAt!).compareTo(DateTime.parse(a.createdAt!));

  static final Comparator<Recipe> timeTotalComparator = (a, b) =>
      (a.timeTotal??0).compareTo(b.timeTotal??0);

  static final Comparator<Recipe> timePrepComparator = (a, b) =>
      (a.timePrep??0).compareTo(b.timePrep??0);

}