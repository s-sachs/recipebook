import 'package:recipe_stornado/models/recipe.dart';

class PlaceholderRecipes {

  static List<Recipe> list = List<Recipe>.generate(20, (e) => dummyRecipe());

  static Recipe dummyRecipe() {
    Recipe recipe = new Recipe();

    recipe.uuid = "Uuid().v1()";
    recipe.ownerId = "Person#1";

    recipe.title = "Dummy Recipe";
    recipe.description = "This is a dummy recipe which is used for development purposes.";
    recipe.originUrl = "local";

    recipe.rating = 3.7;
    recipe.difficulty = 1;

    recipe.timePrep = 10;
    recipe.timeCook = 20;
    recipe.timeTotal = 30;

    recipe.servings = 2;

    recipe.ingredientsSubLists = [
      IngredientsSubList(
        title: "For the chicken",
        ingredients: [
          Ingredient(
            amount: 300.0,
            unit: "g",
            name: "cream",
          ),
          Ingredient(
            amount: 250.0,
            unit: "g",
            name: "chicken",
          ),
          Ingredient(
            amount: 250.0,
            unit: "g",
            name: "chicken",
          ),
          Ingredient(
            amount: 250.0,
            unit: "g",
            name: "chicken",
          ),
          Ingredient(
            name: "salt",
          ),
        ],
      )
    ];

    recipe.instructions = [
      Instruction(
        title: "Prepare the chicken",
        steps: [
          Step(
              number: 1,
              text: "Sear the chicken"
          ),
          Step(
              number: 2,
              text: "Add the cream"
          ),
          Step(
              number: 3,
              text: "Enjoy"
          ),
        ],
      ),
    ];


    recipe.createdAt = DateTime.now().toString();
    recipe.updatedAt = recipe.createdAt;

    recipe.nutrition = ["Alice", "Charlie", "Emma"];
    recipe.tags = ["pig", "trick", "cow"];

    recipe.version = "0.0.1";

    return recipe;
  }

}