import 'package:recipe_stornado/models/recipe.dart';

class RecipeFilters {

  static bool isVegetarian(Recipe recipe) {
    return (recipe.tags??[]).contains("vegetarian");
  }

}