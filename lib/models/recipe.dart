import 'dart:typed_data';

class Recipe {

  int? id;

  String? uuid;
  String? ownerId;

  String? title;
  String? description;
  String? originUrl;

  RecipePicture? picture;

  double? rating;
  double? difficulty;

  int? timePrep;
  int? timeCook;
  int? timeTotal;

  int? servings;

  List<IngredientsSubList>? ingredientsSubLists;

  List<Instruction>? instructions;


  List<String>? tags;
  List<String>? nutrition;

  String? createdAt;
  String? updatedAt;

  String? language;
  String? version;

  Recipe({
    this.uuid,
    this.ownerId,
    this.title,
    this.description,
    this.originUrl,
    this.picture,
    this.rating,
    this.timePrep,
    this.timeCook,
    this.timeTotal,
    this.difficulty,
    this.servings,
    this.ingredientsSubLists,
    this.instructions,
    this.tags,
    this.nutrition,
    this.createdAt,
    this.updatedAt,
    this.language,
    this.version
  });

  Recipe.withId({
      this.id,
      this.ownerId,
      this.title,
      this.description,
      this.originUrl,
      this.picture,
      this.rating,
      this.timePrep,
      this.timeCook,
      this.timeTotal,
      this.difficulty,
      this.servings,
      this.ingredientsSubLists,
      this.instructions,
      this.tags,
      this.nutrition,
      this.createdAt,
      this.updatedAt,
      this.language,
      this.version
  });


  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    if (id != null) map['id'] = id;
    map['recipe_uuid'] = uuid;
    map['owner_uuid'] = ownerId;
    map['title'] = title;
    map['description'] = description;
    map['origin_url'] = originUrl;
    map['rating'] = rating;
    map['difficulty'] = difficulty;
    map['time_prep'] = timePrep;
    map['time_cook'] = timeCook;
    map['time_total'] = timeTotal;
    map['servings'] = servings;
    map['tags'] = (tags??[]).join(",");
    map['nutrition'] = (nutrition??[]).join(",");
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['language'] = language;
    map['version'] = version;
    //map['instructions'] = List<dynamic>.from((instructions??[]).map((x) => x));
    //map['ingredientsSubLists'] = List<dynamic>.from((ingredientsSubLists??[]).map((x) => x));

    return map;
  }

}

class Instruction {

  String? title;

  List<Step>? steps;

  String? comment;

  Instruction({this.title, this.steps, this.comment});

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['title'] = title;
    map['comment'] = comment;
    //map['steps'] = List<dynamic>.from((steps??[]).map((x) => x));

    return map;
  }
}

class Step {

  int? number;

  String? text;

  String? comment;

  Step({this.number, this.text, this.comment});

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['number'] = number;
    map['text'] = text;
    map['comment'] = comment;

    return map;
  }
}

class IngredientsSubList {

  String? title;

  List<Ingredient>? ingredients;

  IngredientsSubList({this.title, this.ingredients});

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['title'] = title;
    //map['ingredients'] = List<dynamic>.from((ingredients??[]).map((x) => x));

    return map;
  }
}

class Ingredient {

  double? amount;

  String? unit;

  String? name;

  String? comment;

  Ingredient({this.amount, this.unit, this.name, this.comment});

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['amount'] = amount;
    map['unit'] = unit;
    map['name'] = name;
    map['comment'] = comment;

    return map;
  }
}

class RecipePicture {

  int? id;
  String? title;
  String? url;
  Uint8List? picture;

  RecipePicture({this.id, this.title, this.url, this.picture});

  RecipePicture.fromMap(Map map):
        id = map["id"],
        title = map["title"],
        url = map["url"],
        picture = map["picture"];

  Map<String, dynamic> toMap() => {
    "id": id,
    "title": title,
    "url": url,
    "picture": picture,
  };

}
